﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

using System.Data;
using MySql.Data.Common;
using MySql.Data.Types;

namespace MySQLDemo.Classes
{
    public class MySQLCustom
    {
        //CHANGE THESE TO MATCH YOUR DB
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "root";
        private static string dbname = "gui_comp6001_16b_assn3";

        //DO NOT CHANGE THESE
        private static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");

            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            //Fixes Encoding - Courtsey of Stackoverflow :-)
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}"});
                        }
                    }
                }
            }

            return listTables;
        } 

        public static List<string> ShowInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return dataList;
        }

        public static string ShowInString(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return string.Join(",", dataList);
        }

      
        //////////////////    DB CLASS ///////////////////////////
        public class AddPerson
        {
            public int id { get; set; }
            public string fname { get; set; }
            public string lname { get; set; }
            public DateTime dob { get; set; }
            public string strAdd { get; set; }
            public string strName { get; set; }
            public string postcode { get; set; }
            public string city { get; set; }
            public string phone1 { get; set; }
            public string phone2 { get; set; }
            public string email { get; set; }


            public AddPerson(int _id, string _fname, string _lname, DateTime _dob, string _strAdd, string _strName, string _postcode, string _city, string _phone1, string _phone2, string _email)
            {
                id = _id;
                fname = _fname;
                lname = _lname;
                dob = _dob;
                strAdd = _strAdd;
                strName = _strName;
                postcode = _postcode;
                city = _city;
                phone1 = _phone1;
                phone2 = _phone2;
                email = _email;
            }

            public AddPerson(string _fname, string _lname, DateTime _dob, string _strAdd, string _strName, string _postcode, string _city, string _phone1, string _phone2, string _email)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                strAdd = _strAdd;
                strName = _strName;
                postcode = _postcode;
                city = _city;
                phone1 = _phone1;
                phone2 = _phone2;
                email = _email;
            }

            public AddPerson(int _id)
            {
                id = _id;
            }
        }

        ////////////////////  INSERT STATEMENTS ///////////////////

        //
        public static void AddData(string fname, string lname, string dob, string strAdd, string strName, string postcode, string city, string phone1, string phone2, string email)
        {
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(fname, lname, date, strAdd, strName, postcode, city, phone1, phone2, email);

            var con = conn();
            con.Open();

            //Get current ID and add increment it to get next id value:
            var command = $"Select MAX(ID) from tbl_people";
            var result = MySQLCustom.ShowInList(command);

            int id;

            if (Int32.TryParse(result[0], out id))
            {

            }
            else
            {
                id = 0;
            }

            id++;

            MySqlCommand insertCommand = con.CreateCommand();

            insertCommand.CommandText = "INSERT INTO tbl_people(ID, FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, POSTCODE, CITY, Phone1, Phone2, EMAIL) VALUES (@ID, @fname, @lname, @dob, @Str_NUMBER, @Str_NAME, @POSTCODE, @CITY, @Phone1, @Phone2, @EMAIL)";

            insertCommand.Parameters.AddWithValue("@ID", id);
            insertCommand.Parameters.AddWithValue("@fname", customdb.fname);
            insertCommand.Parameters.AddWithValue("@lname", customdb.lname);
            insertCommand.Parameters.AddWithValue("@dob", customdb.dob);
            insertCommand.Parameters.AddWithValue("@Str_NUMBER", customdb.strAdd);
            insertCommand.Parameters.AddWithValue("@Str_NAME", customdb.strName);
            insertCommand.Parameters.AddWithValue("@POSTCODE", customdb.postcode);
            insertCommand.Parameters.AddWithValue("@CITY", customdb.city);
            insertCommand.Parameters.AddWithValue("@Phone1", customdb.phone1);
            insertCommand.Parameters.AddWithValue("@Phone2", customdb.phone2);
            insertCommand.Parameters.AddWithValue("@EMAIL", customdb.email);

            insertCommand.ExecuteNonQuery();

            con.Clone();
        }



        ////////////////////  UPDATE STATEMENTS ///////////////////

        //
        public static void UpdateData(string id, string fname, string lname, string dob, string strAdd, string strName, string postcode, string city, string phone1, string phone2, string email)
        {
            var uid = int.Parse(id);
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(uid, fname, lname, date, strAdd, strName, postcode, city, phone1, phone2, email);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //UPDATE TABLE-NAME SET COLUMN = VALUE WHERE COLUMN = VALUE
            //
            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @fname, LNAME = @lname, DOB = @dob, Str_NUMBER = @Str_NUMBER, Str_NAME = @Str_NAME, POSTCODE = @POSTCODE, CITY = @CITY, Phone1 = @Phone1, Phone2 = @Phone2, EMAIL = @EMAIL WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@ID", id);
            updateCommand.Parameters.AddWithValue("@fname", customdb.fname);
            updateCommand.Parameters.AddWithValue("@lname", customdb.lname);
            updateCommand.Parameters.AddWithValue("@dob", customdb.dob);
            updateCommand.Parameters.AddWithValue("@Str_NUMBER", customdb.strAdd);
            updateCommand.Parameters.AddWithValue("@Str_NAME", customdb.strName);
            updateCommand.Parameters.AddWithValue("@POSTCODE", customdb.postcode);
            updateCommand.Parameters.AddWithValue("@CITY", customdb.city);
            updateCommand.Parameters.AddWithValue("@Phone1", customdb.phone1);
            updateCommand.Parameters.AddWithValue("@Phone2", customdb.phone2);
            updateCommand.Parameters.AddWithValue("@EMAIL", customdb.email);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////////////////////  DELETE STATEMENTS ///////////////////

        //
        public static void DeleteData(string id)
        {
            var uid = int.Parse(id);

            var customdb = new AddPerson(uid);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //DELETE FROM TABLE-NAME WHERE COLUM = VALUE

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }
    }
}
