﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Asses3_9968140
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        bool nightMode = false;
        int colour = 0;

        public MainPage()
        {
            this.InitializeComponent();

            //Loads data into list on view pivot
            updateViewListBox();

            //Loads data into list on edit pivot
            updateSelectListBox();

            //Sets the colours of the form
            colourSet();
        }

        private void updateViewListBox()
        {
            string itemToAdd = "";

            //Set the command and executes it and returns a list
            var command = $"Select ID, FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, POSTCODE, CITY, Phone1, Phone2, EMAIL from tbl_people";
            var results = MySQLCustom.ShowInList(command);

            //Creating list to put formatted result into
            var list = new List<string>();

            //Used to change date format from SQL 
            DateTime date;

            //Loops for the items in the list / 11 
            //Each record has 11 attributes and each attribute of each record is an item in the list
            //We only need to loop once for each record not each item
            for (var i = 0; i < results.Count; i = i + 11)
            {
                //Formatting results into a string to be displayed as a listbox item
                itemToAdd = "\nID: "+ results[i];
                itemToAdd += "\nName: " + results[i + 1] + " " + results[i + 2];

                date = DateTime.Parse(results[i + 3]);
                itemToAdd += "\nDate of Birth (DD-MM-YYYY): " + date.ToString("dd-MM-yyyy");

                itemToAdd += "\nAddress:\n";
                itemToAdd += results[i + 4] + " " + results[i + 5] + "\n" + results[i + 6] + "\n" + results[i + 7];

                itemToAdd += "\nContact:\n";
                itemToAdd += results[i + 8] + "\n" + results[i + 9] + "\n" + results[i + 10];
                list.Add(itemToAdd);
            }
            //Setting the data of the listbox on the view page to be the list of formatted results
            listBoxView.ItemsSource = list;
        }

        //Updatting/filling the listbox on the edit page is similar but uses different attributes and formatting
        private void updateSelectListBox()
        {
            listBoxSelect.Items.Clear();

            string itemToAdd = "";

            var command = $"Select ID, FNAME, LNAME from tbl_people";

            var results = MySQLCustom.ShowInList(command);

            var list = new List<string>();

            for (var i = 0; i < results.Count; i = i + 3)
            {
                itemToAdd = results[i] + " " + results[i + 1] + " " + results[i + 2];
                list.Add(itemToAdd);
            }

            //TODO work out why this line crashes the program if updateSelectListBox is called from the add/remove/update button clicked events
            listBoxSelect.ItemsSource = list;
        }


        private void listBoxSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string selection = listBoxSelect.SelectedItem.ToString();
            //Splits the text of the selected item at space, and uses the text before the first space (contact id)
            string id = selection.Split(' ')[0];

            //Sets and executes command to select all infomraiton for a record where the Id matches the contact selected
            var command = "Select * from tbl_people WHERE ID = '" + id + "' ";
            var results = MySQLCustom.ShowInList(command);

            //Filling textboxes with information of selected contact
            //Skip results[0] because that's id and that can't be changed
            textBoxFirstname.Text = results[1];
            textBoxLastname.Text = results[2];

            //Converts date to a nicer format
            DateTime date = DateTime.Parse(results[3]);
            textBoxDob.Text = date.ToString("yyyy-MM-dd");

            textBoxStreetNumber.Text = results[4];
            textBoxStreetName.Text = results[5];
            textBoxPostcode.Text = results[6];
            textBoxCity.Text = results[7];
            textBoxPhone1.Text = results[8];
            textBoxPhone2.Text = results[9];
            textBoxEmail.Text = results[10];
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            messageBox("Contact added");

            //Add Information to the Database
            MySQLCustom.AddData(textBoxFirstname.Text, textBoxLastname.Text, textBoxDob.Text, textBoxStreetNumber.Text, textBoxStreetName.Text, textBoxPostcode.Text, textBoxCity.Text, textBoxPhone1.Text, textBoxPhone2.Text, textBoxEmail.Text);

            clearFields();

            //Refresh view list
            updateViewListBox();

            //Crashes when trying to refresh selection list
            //Crashes when trying to change ItemsSource/add items to the listbox/remove an item/change selected index or almost any other interaction with listboxSelection
            //updateSelectListBox();
        }

        private void buttonUpdate_Click(object sender, RoutedEventArgs e)
        {
            messageBox("Contact updated");

            string selection = listBoxSelect.SelectedItem.ToString();
            //Splits the text of the selected item at space, and uses the text before the first space (contact id)
            string id = selection.Split(' ')[0];

            MySQLCustom.UpdateData(id, textBoxFirstname.Text, textBoxLastname.Text, textBoxDob.Text, textBoxStreetNumber.Text, textBoxStreetName.Text, textBoxPostcode.Text, textBoxCity.Text, textBoxPhone1.Text, textBoxPhone2.Text, textBoxEmail.Text);

            clearFields();

            updateViewListBox();

            //Crashes
            //updateSelectListBox();
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            messageBox("Contact removed");

            string selection = listBoxSelect.SelectedItem.ToString();
            string id = selection.Split(' ')[0];

            MySQLCustom.DeleteData(id);

            clearFields();

            updateViewListBox();

            //Crashes
            //updateSelectListBox();
        }

        static async void messageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Action taken";

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            { }
        }

        private void clearFields()
        {
            textBoxFirstname.Text = "";
            textBoxLastname.Text = "";
            textBoxDob.Text = "";
            textBoxStreetNumber.Text = "";
            textBoxStreetName.Text = "";
            textBoxPostcode.Text = "";
            textBoxCity.Text = "";
            textBoxPhone1.Text = "";
            textBoxPhone2.Text = "";
            textBoxEmail.Text = "";
        }

        private void buttonHamburger_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }

        private void buttonNightDay_Click(object sender, RoutedEventArgs e)
        {
            var black = new SolidColorBrush(Colors.Black);
            var darkGray = new SolidColorBrush(Color.FromArgb(255, 40, 40, 40));
            var darkishGray = new SolidColorBrush(Color.FromArgb(255, 60, 60, 60));
            var white = new SolidColorBrush(Colors.White);

            if (nightMode)
            {
                colourSet();
            }
            else
            {
                nightMode = true;

                pivot.Background = darkishGray;

                //View Pivot
                listBoxView.Background = darkishGray;
                listBoxView.Foreground = white;
                //Edit Pivot
                firstGrid.Background = darkishGray;
                secondGrid.Background = darkGray;
                thirdGrid.Background = darkishGray;

                listBoxSelect.Background = darkishGray;
                listBoxSelect.Foreground = white;

                textBlock3.Foreground = white;
                textBlock4.Foreground = white;
                textBlock5.Foreground = white;
                textBlock6.Foreground = white;
                textBlock7.Foreground = white;
                textBlock8.Foreground = white;
                textBlock9.Foreground = white;
                textBlock10.Foreground = white;
                textBlock11.Foreground = white;
                textBlock12.Foreground = white;

                buttonAdd.Foreground = white;
                buttonUpdate.Foreground = white;
                buttonRemove.Foreground = white;
            }

        }

        private void buttonRGB_Click(object sender, RoutedEventArgs e)
        {
            if (colour != 2)
            {
                colour++;
            }
            else
            {
                colour = 0;
            }

            colourSet();
        }

        private void colourSet()
        {
            nightMode = false;

            var lightBlue = new SolidColorBrush(Color.FromArgb(255, 238, 238, 255));
            var darkBlue = new SolidColorBrush(Color.FromArgb(255, 221, 221, 255));

            var lightOrange = new SolidColorBrush(Color.FromArgb(255, 255, 238, 238));
            var darkOrange = new SolidColorBrush(Color.FromArgb(255, 255, 221, 221));

            var lightGreen = new SolidColorBrush(Color.FromArgb(255, 238, 255, 238));
            var darkGreen = new SolidColorBrush(Color.FromArgb(255, 221, 255, 221));


            if (colour == 0)
            {
                pivot.Background = lightBlue;
                
                //View Pivot
                listBoxView.Background = darkBlue;

                //Edit Pivot
                firstGrid.Background = lightBlue;
                secondGrid.Background = darkBlue;
                thirdGrid.Background = lightBlue;

                listBoxSelect.Background = lightBlue;

                textBlack();
            }
            else if (colour == 1)
            {
                pivot.Background = lightOrange;

                //View Pivot
                listBoxView.Background = darkOrange;

                //Edit Pivot
                firstGrid.Background = lightOrange;
                secondGrid.Background = darkOrange;
                thirdGrid.Background = lightOrange;

                listBoxSelect.Background = lightOrange;

                textBlack();
            }
            else if (colour == 2)
            {
                pivot.Background = lightGreen;

                //View Pivot
                listBoxView.Background = darkGreen;

                //Edit Pivot
                firstGrid.Background = lightGreen;
                secondGrid.Background = darkGreen;
                thirdGrid.Background = lightGreen;

                listBoxSelect.Background = lightGreen;

                textBlack();
            }
        }

        private void textBlack()
        {
            var black = new SolidColorBrush(Colors.Black);

            listBoxView.Foreground = black;
            listBoxSelect.Foreground = black;

            textBlock3.Foreground = black;
            textBlock4.Foreground = black;
            textBlock5.Foreground = black;
            textBlock6.Foreground = black;
            textBlock7.Foreground = black;
            textBlock8.Foreground = black;
            textBlock9.Foreground = black;
            textBlock10.Foreground = black;
            textBlock11.Foreground = black;
            textBlock12.Foreground = black;

            buttonAdd.Foreground = black;
            buttonUpdate.Foreground = black;
            buttonRemove.Foreground = black;
        }
    }
}
